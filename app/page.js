'use client'
import CardMenu from "@/components/CardMenu"
import Image from "next/image"
import { useEffect, useState } from "react"
import ArrowDown from "@/public/Icon/arrow-down.svg"
import axios from "axios"
import { useRouter } from "next/router"

export default function Home() {

  const [menu, setMenu] = useState('Hot Dishes')
  const [foodData, setFoodData] = useState([])

  useEffect(() => {
    getFood()
  }, [menu])

  const getFood = async () => {
    try {
      await axios.get(`http://localhost:3222/food?page=1&limit=100&menu=${menu}`)
      .then((response) => {
        setFoodData(response.data.items)
      })
    } catch (error) {
      console.log(error)      
    }
  }
  
  return (
    <div className="w-full">
      <div className="flex gap-8 items-center">
        <div className={`${menu === 'Hot Dishes' ? 'text-primary-color-67' : 'text-white'} cursor-pointer text-sm font-semibold`} onClick={() => setMenu('Hot Dishes')}>
          <span>Hot Dishes</span>
          <div className={`${menu === 'Hot Dishes' ? 'bg-primary-color-67' : 'bg-none'} h-0.5 w-10 mt-1`}></div>
        </div>
        <div className={`${menu === 'Cold Dishes' ? 'text-primary-color-67' : 'text-white'} cursor-pointer text-sm font-semibold`} onClick={() => setMenu('Cold Dishes')}>
          <span>Cold Dishes</span>
          <div className={`${menu === 'Cold Dishes' ? 'bg-primary-color-67' : 'bg-none'} h-0.5 w-10 mt-1`}></div>
        </div>
        <div className={`${menu === 'Soup' ? 'text-primary-color-67' : 'text-white'} cursor-pointer text-sm font-semibold`} onClick={() => setMenu('Soup')}>
          <span>Soup</span>
          <div className={`${menu === 'Soup' ? 'bg-primary-color-67' : 'bg-none'} h-0.5 w-10 mt-1`}></div>
        </div>
        <div className={`${menu === 'Grill' ? 'text-primary-color-67' : 'text-white'} cursor-pointer text-sm font-semibold`} onClick={() => setMenu('Grill')}>
          <span>Grill</span>
          <div className={`${menu === 'Grill' ? 'bg-primary-color-67' : 'bg-none'} h-0.5 w-10 mt-1`}></div>
        </div>
        <div className={`${menu === 'Appetizer' ? 'text-primary-color-67' : 'text-white'} cursor-pointer text-sm font-semibold`} onClick={() => setMenu('Appetizer')}>
          <span>Appetizer</span>
          <div className={`${menu === 'Appetizer' ? 'bg-primary-color-67' : 'bg-none'} h-0.5 w-10 mt-1`}></div>
        </div>
        <div className={`${menu === 'Dissert' ? 'text-primary-color-67' : 'text-white'} cursor-pointer text-sm font-semibold`} onClick={() => setMenu('Dissert')}>
          <span>Dissert</span>
          <div className={`${menu === 'Dissert' ? 'bg-primary-color-67' : 'bg-none'} h-0.5 w-10 mt-1`}></div>
        </div>
      </div>
      <div className="mt-9">
        <div className="flex justify-between items-center">
          <span className="text-white font-semibold text-xl">Choose Dishes</span>
          <div className="border border-base-dark-line bg-base-dark-bg-2 py-4 w-52 flex items-center gap-3 px-6 rounded-lg cursor-pointer h-fit">
            <Image alt="icon" src={ArrowDown} />
            <span>Cheapers</span>
          </div>
        </div>
        <div className="overflow-y-auto h-full mt-6 grid grid-cols-5 gap-6">
          {
            menu === "Hot Dishes" ? foodData.map((result, index) => {
              return <CardMenu key={index} foodName={result.name} price={result.price} stock={`${result.stock} Bowls available`} imgPath={`http://localhost:3222/image/food/${result?.img}`} idFood={result.id} />
            }) : menu === "Cold Dishes" ? foodData.map((result, index) => {
              return <CardMenu key={index} foodName={result.name} price={result.price} stock={`${result.stock} Bowls available`} imgPath={`http://localhost:3222/image/food/${result?.img}`} idFood={result.id} />
            }) : menu === "Soup" ? foodData.map((result, index) => {
              return <CardMenu key={index} foodName={result.name} price={result.price} stock={`${result.stock} Bowls available`} imgPath={`http://localhost:3222/image/food/${result?.img}`} idFood={result.id} />
            }) : menu === "Grill" ? foodData.map((result, index) => {
              return <CardMenu key={index} foodName="Melon, prosciutto and pea salad" price="$ 2.29" stock="20 Bowls available" />
            }) : menu === "Appetizer" ? foodData.map((result, index) => {
              return <CardMenu key={index} foodName="Spicy seasoned seafood noodles" price="$ 2.29" stock="20 Bowls available" />
            }) : menu === "Dissert" ? foodData.map((result, index) => {
              return <CardMenu key={index} foodName="Spicy seasoned seafood noodles" price="$ 2.29" stock="20 Bowls available" />
            }) : (
              <div></div>
            )
          }
        </div>
      </div>
    </div>
  )
}
