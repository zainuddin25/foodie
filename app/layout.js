'use client'
import { useEffect, useState } from 'react'
import './globals.css'
import { usePathname } from 'next/navigation' 
import LeftSidebar from '@/components/LeftSidebar'
import RightSidebar from '@/components/RightSidebar'
import Header from '@/components/Header'

export default function RootLayout({ children }) {

  const pathName = usePathname()
  const [path, setPath] = useState('')
  const [status, setStatus] = useState(true)
 
  // useEffect(() => {
  //   if (pathName.includes("auth")) {
  //     setStatus(false)
  //   } else {
  //     setStatus(true)
  //   }
  // }, [])

  return (
    <html lang="en">
      <head>
        <title>Foodie</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
        <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"></link>
      </head>
      <body>
          {/* {status === false ? (
            <div className='w-screen h-screen'>
              {children}
            </div>
          ) : ( */}
            <div className='flex h-screen'>
              <div className='w-[7%]'>
                <LeftSidebar />
              </div>
              <div className='w-[86%]'>
                <div className='h-[10%] flex items-center'>
                  <Header title={path} />
                </div>
                <div className='h-[90%] px-4 py-2 overflow-y-auto' id='menu'>
                  {children}
                </div>
              </div>
              <div className='w-[7%]'>
                <RightSidebar />
              </div>
            </div>
          {/* )} */}
      </body>
    </html>
  )
}
