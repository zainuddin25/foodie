const { default: axios } = require("axios");

const api = axios.create({
    baseURL: 'http://localhost:3222'
})

export default api