/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  server: {
    // Konfigurasi untuk mengizinkan request HTTP
    http: {
      // Gunakan port yang sesuai dengan API lokal kamu
      port: process.env.PORT || 3000,
      // Atur flag untuk mengaktifkan protokol HTTP
      noH2: true
    }
  },
  env : {
    API_GET_FOOD : "http://localhost:3222/food?page=1&limit=100&menu=Hot%20Dishes"
  },
  async headers() {
    return [
      {
        source: "/api/:path*",
        headers: [{
          key: 'Access-Control-Allow-Origin',
          value: '*'
        }]
      }
    ]
  },
  images: {
    domains: ['localhost'],
    formats: ['image/avif', 'image/webp']
  }
}

module.exports = nextConfig
