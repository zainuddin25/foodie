/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
 
    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary-color-67' : '#EA736D',
        'primary-color-73' : '#EA736D',
        'secondary-color' : '#4658BC',
        'white' : '#fff',
        'text-dark' : '#3B5162',
        'text-gray' : '#889898',
        'text-light' : '#ABBBC2',
        'text-light-90' : '#E0E6E9',
        'base-dark-line' : '#393C49',
        'base-bg' : '#fafafa',
        'base-dark-bg-1' : '#252836',
        'base-dark-bg-2' : '#1F1D2B',
        'base-form-bg' : '#2D303E',
        'accents-green' : '#50D1AA',
        'accents-red' : '#FF7CA3',
        'accents-orange' : '#FFB572',
        'accents-blue' : '#65B0F6',
        'accents-purple' : '#9290FE',
      },
      dropShadow: {
        'primary-shadow' : '0 8px 10px rgba(234, 124, 105, 0.32)'
      }
    },
  },
  plugins: [],
}

