import { Modal } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";

export default function ModalAddCart(props) {

    const { idFood } = props
    const [foodDetail, setFoodDetail] = useState()
    const [mount, setMount] = useState(0)
    const [totalPrice, setTotalPrice] = useState(0)
    const [message, setMessage] = useState()

    useEffect(() => {
        getDetailFood()
    },[])

    const getDetailFood = async () => {
        try {
            await axios.get(`http://localhost:3222/food/${idFood}`)
            .then((response) => {
                setFoodDetail(response.data.data)
            })
        } catch (error) {
            console.log(error)            
        }
    }

    return (
        <>
            
        </>
    )
}