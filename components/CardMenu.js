import { useEffect, useState } from "react";
import { Modal, message } from "antd";
import axios from "axios";

export default function CardMenu(props) {

    const { foodName, price, stock, imgPath, idFood } = props
    const [foodDetail, setFoodDetail] = useState()
    const [mount, setMount] = useState(0)
    const [messageOrder, setMessage] = useState()

    const [modal, setModal] = useState(false)

    const openModal = async () => {
        setModal(true)
        try {
            await axios.get(`http://localhost:3222/food/${idFood}`)
            .then((response) => {
                setFoodDetail(response.data.data)
            })
        } catch (error) {
            console.log(error)            
        }
    }

    const getCart = async () => {
        const idUser = localStorage.getItem("user")
        try {
            await axios.get(`http://localhost:3222/cart/user/${idUser}`)
        } catch (error) {
            console.log(error)
        }
    }

    const handleCreateCart = async () => {
        try {
            const data = {
                message: messageOrder,
                count: mount,
                idFood: idFood
            }

            await axios.post('http://localhost:3222/cart', data)

            .then((response) => {
                getCart()
                message.success('Success Add To Cart')
                setModal(false)
            })

        } catch (error) {
            console.log(error)
        }
    }

    const handlePlus = () => {
        if(mount == foodDetail.stock) {
            setMount(foodDetail.stock)
        } else {
            setMount(mount + 1)
        }
    }

    const handleMinus = () => {
        if(mount == 0) {
            setMount(0)
        } else {
            setMount(mount - 1)
        }
    }

    return (
        <div className="relative w-full flex flex-col items-center justify-center">
            <img src={imgPath} alt="food" className="z-20 rounded-full" width={132} height={132} />
            <div className="bg-base-dark-bg-2 w-full pt-28 px-4 pb-4 rounded-2xl flex flex-col items-center -translate-y-16 z-10">
                <span className="text-xl font-medium text-center">{foodName}</span>
                <span className="text-base font-normal pt-2 pb-1 text-center">${price}</span>
                <span className="text-base font-normal text-center text-text-light">{stock}</span>
                <button className="mt-4 py-3 bg-primary-color-67 w-full rounded-lg" onClick={() => openModal()}>Order Now</button>
                <Modal
                    style={{
                        top: 20,
                    }}
                    open={modal}
                    onOk={() => handleCreateCart()}
                    onCancel={() => setModal(false)}
                    footer={[
                        <div className="w-full flex justify-between items-center gap-4">
                            <button className="w-2/4 border border-primary-color-67 py-3 text-base font-semibold text-primary-color-67 rounded-lg" onClick={() => setModal(false)}>
                                Cancle
                            </button>
                            <button className="w-2/4 border bg-primary-color-67 py-3 border-none text-base font-semibold text-white rounded-lg" onClick={() => handleCreateCart()}>
                                Add To Cart
                            </button>
                        </div>
                    ]}
                >
                    <div className="py-6 w-full text-white">
                        <div className="flex justify-between items-start mt-4 gap-4">
                            <div className="w-1/3">
                                <img src={`http://localhost:3222/image/food/${foodDetail?.img}`} className="rounded-full w-full" />
                            </div>
                            <div className="w-2/3 flex flex-col">
                                <span className="text-xl font-bold">{foodDetail?.name}</span>
                                <span className="text-base font-medium">Ready Stock : {foodDetail?.stock}</span>
                                <span className="text-base font-medium">Price : {foodDetail?.price}</span>
                            </div>
                        </div>
                        <div className="grid grid-cols-3 mt-8 gap-4">
                            <div className="w-full col-span-1 bg-base-dark-bg-1 border border-base-dark-line rounded-lg">
                                <div className="flex items-center">
                                    <div className="flex justify-center items-center w-[15%] cursor-pointer" onClick={() => handleMinus()}>
                                        <span className="text-base font-semibold">-</span>
                                    </div>
                                    <div className="flex justify-center items-center w-[70%]">
                                        <span className="text-base font-semibold py-4 text-white">{mount}</span>
                                    </div>
                                    <div className="flex justify-center items-center w-[15%] cursor-pointer" onClick={() => handlePlus()}>
                                        <span className="text-base font-semibold">+</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-span-2 flex justify-center items-center w-full">
                                <input placeholder="Message" className="w-full h-full bg-base-dark-bg-1 border border-base-dark-line rounded-lg px-4 text-base font-semibold" onChange={(e) => setMessage(e?.target?.value)} />
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
    )
}