import Image from "next/image";
import Logo from "@/public/Icon/logo.svg"
import LogOut from "@/public/Icon/log-out.svg"
import HomeActive from "@/public/Icon/home-active.svg"
import HomeNonActive from "@/public/Icon/home-non-active.svg"
import DiscountActive from "@/public/Icon/discount-active.svg"
import DiscountNonActive from "@/public/Icon/discount-non-active.svg"
import DashboardActive from "@/public/Icon/dashboard-active.svg"
import DashboardNonActive from "@/public/Icon/dashboard-non-active.svg"
import NotificationActive from "@/public/Icon/notification-active.svg"
import NotificationNonActive from "@/public/Icon/notification-non-active.svg"
import OrderActive from "@/public/Icon/order-active.svg"
import OrderNonActive from "@/public/Icon/order-non-active.svg"
import CustomerActive from "@/public/Icon/customer-active.svg"
import CustomerNonActive from "@/public/Icon/customer-non-active.svg"
import SettingActive from "@/public/Icon/setting-active.svg"
import SettingNonActive from "@/public/Icon/setting-non-active.svg"
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import Link from "next/link"

export default function LeftSidebar() {

    const pathName = usePathname()

    return (
        <div className="h-full w-full bg-base-dark-bg-2 rounded-r-2xl">
            <div className="h-[10%] w-full flex justify-center items-center">
                <div className="p-2 bg-accents-orange bg-opacity-30 rounded-xl">
                    <Image src={Logo} alt="logo" />
                </div>
            </div>
            <div className="h-[80%] pl-2 py-4 overflow-y-auto" id="sidebar-menu">
                <Link href="/">
                    <div className={`${pathName === '/' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 py-6`}>
                        <div className={`${pathName === '/' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/' ? (
                                    <Image className="w-full" src={HomeActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={HomeNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
                <Link href="/discount">
                    <div className={`${pathName === '/discount' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 p-6`}>
                        <div className={`${pathName === '/discount' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/discount' ? (
                                    <Image className="w-full" src={DiscountActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={DiscountNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/discount' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/discount' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/discount' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/discount' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
                <Link href="/dashboard">
                    <div className={`${pathName === '/dashboard' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 p-6`}>
                        <div className={`${pathName === '/dashboard' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/dashboard' ? (
                                    <Image className="w-full" src={DashboardActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={DashboardNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/dashboard' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/dashboard' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/dashboard' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/dashboard' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
                <Link href="/notification">
                    <div className={`${pathName === '/notification' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 p-6`}>
                        <div className={`${pathName === '/notification' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/notification' ? (
                                    <Image className="w-full" src={NotificationActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={NotificationNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/notification' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/notification' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/notification' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/notification' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
                <Link href="/order">
                    <div className={`${pathName === '/order' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 p-6`}>
                        <div className={`${pathName === '/order' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/order' ? (
                                    <Image className="w-full" src={OrderActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={OrderNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/order' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/order' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/order' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/order' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
                <Link href="/customer">
                    <div className={`${pathName === '/customer' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 p-6`}>
                        <div className={`${pathName === '/customer' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/customer' ? (
                                    <Image className="w-full" src={CustomerActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={CustomerNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/customer' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/customer' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/customer' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/customer' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
                <Link href="/setting">
                    <div className={`${pathName === '/setting' ? 'bg-base-dark-bg-1' : 'bg-base-dark-bg-2' } flex justify-center items-center rounded-l-xl relative mb-5 p-6`}>
                        <div className={`${pathName === '/setting' ? 'bg-primary-color-67 drop-shadow-primary-shadow' : 'bg-none drop-shadow-none'} p-4 flex justify-center items-center rounded-lg`}>
                            {
                                pathName === '/setting' ? (
                                    <Image className="w-full" src={SettingActive} alt="icon" />
                                ) : (
                                    <Image className="w-full" src={SettingNonActive} alt="icon" />
                                )
                            }
                        </div>
                        <div className={`${pathName === '/setting' ? 'flex' : 'hidden'} absolute -top-5 rounded-br-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/setting' ? 'flex' : 'hidden'} absolute -top-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                        <div className={`${pathName === '/setting' ? 'flex' : 'hidden'} absolute -bottom-5 rounded-tr-2xl right-0 h-5 w-5 bg-base-dark-bg-2 z-20`}></div>
                        <div className={`${pathName === '/setting' ? 'flex' : 'hidden'} absolute -bottom-5 right-0 h-5 w-5 bg-base-dark-bg-1 z-10`}></div>
                    </div>
                </Link>
            </div>
            <div className="h-[10%] w-full flex justify-center items-center">
                <div className="">
                    <Image src={LogOut} alt="icon" />
                </div>
            </div>
        </div>
    )
}