import Image from "next/image";
import searchIcon from "@/public/Icon/search.svg"

export default function Header(props) {

    const today = new Date();
    const day = today.getDate();
    const monthIndex = today.getMonth() + 1;
    const year = today.getFullYear()

    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    
    const monthName = months[monthIndex];

    const dayofweek = today.toLocaleDateString("en-US", { weekday: "long" })

    return (
        <div className="flex justify-between items-center w-[86%] px-4 fixed">
            <div className="flex flex-col h-full justify-center w-fit">
                <span className="text-2xl font-semibold">{props.title}</span>
                <span className="text-base">{dayofweek}, {day} {monthName} {year}</span>
            </div>
            <div className="relative">
                <input className="bg-base-form-bg py-3 pl-8 pr-3 w-60 outline-none border border-base-dark-line rounded-lg text-sm" placeholder="Search for food, coffe, etc..." />
                <div className="absolute top-3 left-2">
                    <Image src={searchIcon} alt="icon" />
                </div>
            </div>
        </div>
    )
}