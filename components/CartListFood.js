import Image from "next/image"
import DeleteOutlined from "@ant-design/icons"
import axios from "axios"
import { ExclamationCircleFilled } from '@ant-design/icons';
import { useEffect, useState } from "react"
import { Button, Modal, Space } from 'antd';
const { confirm } = Modal;

const CartListFood = () => {

    const [cart, setCart] = useState([])
    const idUser = localStorage.getItem('user')

    useEffect(() => {
        getCart()
    }, [])

    const getCart = async () => {
        try {
            await axios.get(`http://localhost:3222/cart/user/${idUser}`)
            .then((response) => {
                setCart(response.data.data)
            })
        } catch (error) {
            console.log(error)
        }
    }
    
    const deleteCart = async (idCart) => {
        try{
            await axios.delete(`http://localhost:3222/cart/${idCart}`)
            .then((response) => {
                if (response.data.statusCode === 200) {
                    getCart()
                }
            })
        } catch (error) {
            console.log(error)
        }
    }

    const showDeleteConfirm = async (idCart) => {
        confirm({
          title: <span className="text-white">Are you sure you want to remove from cart ?</span>,
          icon: <ExclamationCircleFilled />,
          okText: 'Yes',
          okType: 'danger',
          cancelText: 'No',
          onOk() {
            deleteCart(idCart)
          },
          onCancel() {
            console.log('Cancel');
          },
        });
    };

    return (
        <div>
            {cart.length == 0 ? (
                <span className="text-base font-semibold">No Food in Cart</span>
            ) : (
                <div>
                    <div className="py-2 grid grid-cols-6 border-b border-base-dark-line">
                        <div className="col-span-4">
                            <span className="text-base font-semibold">Item</span>
                        </div>
                        <div className="col-span-1 flex justify-center">
                            <span className="text-base font-semibold">Qty</span>
                        </div>
                        <div className="col-span-1 flex justify-center">
                            <span className="text-base font-semibold">Price</span>
                        </div>
                    </div>
                    <div className="grid grid-cols-6 mt-4 gap-y-3 gap-x-2">
                        {cart?.map((result, index) => {
                            return (
                                <>
                                    <div className="col-span-4 flex items-center" key={index}>
                                        <div className="flex items-center gap-4">
                                            <img src={`http://localhost:3222/image/food/${result?.food?.img}`} width={46} height={46} />
                                            <div className="flex flex-col w-[70%]">
                                                <span className="font-medium text-sm truncate">{result?.food?.name}</span>
                                                <span className="text-text-light text-xs">$ {result?.food?.price}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w-full rounded-lg border border-base-dark-line bg-base-dark-bg-1 py-3 flex justify-center items-center">
                                        <span className="text-base font-medium text-white">{result?.count}</span>
                                    </div>
                                    <div className="w-full py-3 flex justify-center items-center">
                                        <span className="text-base font-medium text-white">$ {result?.food?.price * result?.count}</span>
                                    </div>
                                    <div className="col-span-5 bg-base-dark-bg-1 p-3 rounded-lg border border-base-dark-line">
                                        <span className="text-sm text-text-light-90">{result?.message}</span>
                                    </div>
                                    <div className="w-full flex justify-center items-center border border-primary-color-67 rounded-lg cursor-pointer" onClick={() => showDeleteConfirm(result?.id)}>
                                        <img src={'/icon/trash.svg'} />
                                    </div>
                                </>
                            )
                        })}
                    </div>
                </div>
            )}
        </div>
    )
}

export default CartListFood