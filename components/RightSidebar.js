import Image from "next/image";
import Cart from "@/public/Icon/cart.svg"
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import CartListFood from "./CartListFood";
import axios from "axios";

export default function RightSidebar() {

    const [open, setOpen] = useState(false)
    const [cartData, setDataCart] = useState([])
    const tempTotalPrice = []
    const [totalPrice, settTotalPrice] = useState()

    const pathName = usePathname()

    useEffect(() => {
        getCart()
    }, [])

    useEffect(() => {
        cartData.map(result => {
            tempTotalPrice.push(parseInt(result?.count * result?.food.price))
            let total = 0
            for (let i = 0; i < tempTotalPrice.length; i++) {
                total += tempTotalPrice[i]
            }
            settTotalPrice(total)
        })
    }, [])

    const getCart = async () => {
        const idUser = localStorage.getItem('user')
        try {
            await axios.get(`http://localhost:3222/cart/user/${idUser}`)
            .then((result) => {
                setDataCart(result.data.data)
            })
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div className="h-full w-full bg-base-dark-bg-2 rounded-r-2xl">
            <div className="h-[10%] w-full flex justify-center items-center cursor-pointer" onClick={() => setOpen(true)}>
                <div className="p-4 bg-primary-color-67 rounded-xl">
                    <Image src={Cart} alt="icon" />
                </div>
            </div>
            <div className={`absolute top-0 right-0 h-screen bg-base-dark-bg-2 p-6 w-[400px] ${open === true ? 'block' : 'hidden'}`}>
                <div className="flex flex-col h-[15%]">
                    <span className="font-medium text-xl">Orders</span>
                    <button className="bg-primary-color-67 w-fit mt-6 px-3 py-2 rounded-lg">Delivery</button>
                </div>
                <div className="h-[70%] overflow-y-scroll pb-6" id="cart-scrollbar">
                    <CartListFood />
                </div>
                <div className="h-[15%] flex flex-col justify-between">
                    <div className="">
                        <div className="flex justify-between items-center">
                            <span className="text-sm text-text-light">Discount</span>
                            <span className="text-base font-medium text-white">$0</span>
                        </div>
                        <div className="flex justify-between items-center">
                            <span className="text-sm text-text-light">Sub Total</span>
                            <span className="text-base font-medium text-white">${totalPrice}</span>
                        </div>
                    </div>
                    <div className="flex justify-between items-center gap-3">
                        <button className="w-2/4 border border-primary-color-67 rounded-lg py-3 text-primary-color-67 font-semibold" onClick={() => setOpen(false)}>
                            Go Back
                        </button>
                        <button className="w-2/4 bg-primary-color-67 rounded-lg py-3 text-white font-semibold">
                            Continue Payment
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}